import mongoose, { Connection } from 'mongoose';
global.beforeAll(async () => {
  try {
    await deleteDbTest();
  } catch (err) {
    console.error(err)
  }
});

global.afterAll(async () => {
  await deleteDbTest();
});

async function deleteDbTest() {
  const conn = await mongoose.connect(process.env.DATABASE_TEST_URL);
  await conn.connection.db.dropDatabase();
  await conn.connection.close();

}
