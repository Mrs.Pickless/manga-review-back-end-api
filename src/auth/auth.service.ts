import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { User, UserDocument } from '../users/entities/user.entity';
import { UsersService } from '../users/users.service';
import { randomBytes, scrypt as _scrypt } from 'crypto';

import { promisify } from 'util';
import mongoose from 'mongoose';

const scrypt = promisify(_scrypt);

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) { }

  /**
   * Method used to validate if provided user correspond to a user saved in bdd
   * - we verify if email exist
   * - if user exist then we extract stored hash of his password
   *  - we hash the provided password and check if it is the same
   */
  async validateUser(
    email: string,
    pass: string,
  ): Promise<UserDocument | null> {
    const user = await this.usersService.findByEmail(email);

    if (!user) {
      throw new NotFoundException('user not found for this email');
    }
    const [salt, storedHash] = user.password.split('.');
    const hash = (await scrypt(pass, salt, 32)) as Buffer;
    if (storedHash !== hash.toString('hex')) {
      throw new BadRequestException('bad password');
    }

    return user;
  }

  /**
   * create jwt and delete password from user object
   * @return all user data exept his password and access_token that expire in 1 day
   */
  async login(user: UserDocument) {
    const userToReturn = user.toJSON();
    delete userToReturn.password;
    return {
      access_token: this.jwtService.sign(userToReturn, { expiresIn: '1d' }),
      user: userToReturn,
    };
  }
}
