import { UseGuards, Controller, Get, Post, Body, Patch, Param, Delete, Request } from '@nestjs/common';
import { ManganimeService } from './manganime.service';
import { ApiResponse, ApiBearerAuth,ApiTags } from '@nestjs/swagger';
import { CreateManganimeDto } from './dto/create-manganime.dto';
import { UpdateManganimeDto } from './dto/update-manganime.dto';
import { JwtAuthGuard } from '../auth/authGuards/jwt-auth.guard';

/**
 * This class is the manganime controller used to routing
 */
@ApiTags('manganime')
@Controller('manganime')
export class ManganimeController {
  constructor(private readonly manganimeService: ManganimeService) {}

  /**
   * Post request to create a manganime
   * @example [POST] https://example/manganime
   */
  @ApiResponse({ status: 201, description: 'The manganime has been successfully created.'})
  @ApiResponse({ status: 400, description: 'Bad Request'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Request() req, @Body() createManganimeDto: CreateManganimeDto) {
    return this.manganimeService.create(req.user, createManganimeDto);
  }

  /**
   * Get request to find all the manganimes
   * @example [GET] https://example/manganime
   */
  @ApiResponse({ status: 200, description: 'The manganimes has been successfully requested.'})
  @Get()
  findAll() {
    return this.manganimeService.findAll();
  }

  /**
   * Get request to find all manganime from authorId
   * @example [GET] https://example/manganime/author/id
   */
  @ApiResponse({ status: 200, description: 'The manganimes has been successfully requested.'})
  @Get('/author/:id')
  findAllByAuthor(@Param('id') id: string) {
    return this.manganimeService.findAllByAuthor(id);
  }
   

  /**
   * Get request to find one manganime by its Id
   * @example [GET] https://example/manganime/id
   */
  @ApiResponse({ status: 200, description: 'The manganime has been successfully requested.'})
  @ApiResponse({ status: 400, description: 'Bad Request'})
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.manganimeService.findOne(id);
  }

  /**
   * Patch request to update one manganime by its Id
   * @example [PATCH] https://example/manganime/id
   */
  @ApiResponse({ status: 200, description: 'The manganime has been successfully updated.'})
  @ApiResponse({ status: 400, description: 'Bad Request'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  update(@Request() req, @Param('id') id: string, @Body() updateManganimeDto: UpdateManganimeDto) {
    return this.manganimeService.update(req.user, id, updateManganimeDto);
  }

  /**
   * Delete request to delete one manganime by its Id
   * @example [DELETE] https://example/manganime/id
   */
  @ApiResponse({ status: 200, description: 'The manganime has been successfully deleted.'})
  @ApiResponse({ status: 400, description: 'Bad Request'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  remove(@Request() req, @Param('id') id: string) {
    return this.manganimeService.remove(req.user, id);
  }
}
