import { PartialType } from '@nestjs/swagger';
import { CreateGenreDto } from './create-genre.dto';

/**
* This class is used to update the genre
*/
export class UpdateGenreDto extends PartialType(CreateGenreDto) {}
