import {
  UnauthorizedException,
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Manganime, ManganimeDocument } from '../manganime/entities/manganime.entity';
import { Comment, CommentDocument } from './entities/comment.entity';
import { CreateCommentDto } from './dto/create-comment.dto';
import { UpdateCommentDto } from './dto/update-comment.dto';
import { UserDocument } from '../users/entities/user.entity';

/**
 * This class is the comment service to handle all comments methods
 */
@Injectable()
export class CommentsService {
  constructor(@InjectModel(Comment.name) private commentModel: Model<CommentDocument>, @InjectModel(Manganime.name) private manganimeModel: Model<ManganimeDocument>) { }

  /**
  * This method handle the creation of a comment
  */
  async create(user: UserDocument, createCommentDto: CreateCommentDto): Promise<Partial<Comment>> {
    createCommentDto['authorId'] = user._id;
    if(!createCommentDto['manganimeId'].match(/^[0-9a-fA-F]{24}$/)) throw new BadRequestException("The manganime id is not correct.");
    
    const manganimeToReturn =  await this.manganimeModel.findById(createCommentDto['manganimeId']).lean();
    if (manganimeToReturn == null) throw new BadRequestException("The requested manga does not exist.");

    const commentToCreate = new this.commentModel(createCommentDto);
    const createdComment = await commentToCreate.save();
    const commentToReturn = createdComment.toJSON();
    return commentToReturn;
  }

  /**
  * This method returns all comments from author id
  */
  async findAllByAuthor(id: string) {
    const commentsToReturn = await this.commentModel.find({authorId: id}).populate('manganimeId').lean();
    return commentsToReturn;
  }

  /**
  * This method handle the update of a comment
  */
  async update(user: UserDocument, id: string, updateCommentDto: UpdateCommentDto) {
    const checkComment =  await this.commentModel.findById(id);
    if(checkComment == null) throw new BadRequestException("The requested manga does not exist.");

    if(!user.isAdmin && user._id !== checkComment.authorId) throw new UnauthorizedException("You're not authorized to do that.");

    const commentToReturn = await this.commentModel.findByIdAndUpdate({ _id: id.toString() }, updateCommentDto, { new: true })
    if (commentToReturn == null) throw new BadRequestException("The requested comment does not exist.");
    return commentToReturn;
  }

  /**
  * This method handle the deletion of a comment
  */
  async remove(user: UserDocument, id: string) {
    const checkComment =  await this.commentModel.findById(id);
    if(checkComment == null) throw new BadRequestException("The requested manga does not exist.");

    if(!user.isAdmin && user._id !== checkComment.authorId) throw new UnauthorizedException("You're not authorized to do that.");
    
    const commentToReturn =  await this.commentModel.findByIdAndDelete(id)
    if (commentToReturn == null) throw new BadRequestException("The requested manga does not exist.");
    return commentToReturn;
  }
}
