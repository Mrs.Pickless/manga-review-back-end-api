import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type CommentDocument = Comment & Document;

/**
 * This class is the comment entity used for mongoose
 */
@Schema({ collection: 'comments' })
export class Comment {
  /**
  * id of the manganime
  * @example "62bd57d9f1be9ce48b3c9fbc"
  */
  @Prop({ref: 'Manganime'})
  manganimeId: string;

  /**
  * id of the author
  * @example "62bd57d9f1be9ce48b3c9fbc"
  */
  @Prop({ref: 'User'})
  authorId: string;

  /**
  * Content of the comment
  * @example "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi hendrerit nunc arcu, rutrum elementum lacus porttitor et. Praesent turpis neque, iaculis a blandit non, convallis quis purus. Nullam in libero efficitur nulla fermentum imperdiet."
  */
  @Prop()
  content: string;

  /**
  * creation date timestamp of the comment
  * @example 1656426188913
  */
  @Prop({ type: Number, default: Date.now() })
  createdAt: number;
}

export const CommentSchema = SchemaFactory.createForClass(Comment);