import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Date, Document } from 'mongoose';

export type UserDocument = User & Document;

@Schema()
export class User {
  @Prop()
  email: string;

  @Prop()
  password: string;

  @Prop()
  username: string;

  @Prop({ type: Boolean, default: false })
  isAdmin: Boolean;

  @Prop({ type: Number, default: Date.now() })
  createdAt: number;

  @Prop()
  imageUrl?: string;

  @Prop()
  description: string;
}

export const UserSchema = SchemaFactory.createForClass(User);
